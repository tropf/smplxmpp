# automatically calculates patches since last major.minor version
set(GIT_VERSION_SUCCESS FALSE)
set(VERSION_DATE "1612709109")

option(SMPLXMPP_VERSION_GUESS_GIT "will try to use git to count commits since last version and append that to the version number" ON)

set(SMPLXMPP_VERSION "${PROJECT_VERSION}" CACHE STRING "version of smplxmpp")
set(SMPLXMPP_VERSION_DATE "${VERSION_DATE}" CACHE STRING "timestamp to use for date of version")

if (SMPLXMPP_VERSION_GUESS_GIT)
    find_package(Git)
    if(GIT_FOUND)
        execute_process(COMMAND ${GIT_EXECUTABLE} "rev-list" "--count" "v${PROJECT_VERSION}..HEAD"
                        WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
                        OUTPUT_VARIABLE GIT_COMMIT_COUNT
                        RESULT_VARIABLE GIT_COMMIT_COUNT_RETURN_VAR
                        OUTPUT_STRIP_TRAILING_WHITESPACE
                        ERROR_QUIET)

        execute_process(COMMAND ${GIT_EXECUTABLE} "rev-parse" "--short" "HEAD"
                        WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
                        OUTPUT_VARIABLE GIT_COMMIT_HASH
                        RESULT_VARIABLE GIT_COMMIT_HASH_RETURN_VAR
                        OUTPUT_STRIP_TRAILING_WHITESPACE
                        ERROR_QUIET)

        if ("0" EQUAL "${GIT_COMMIT_COUNT_RETURN_VAR}")
            if(NOT "0" EQUAL "${GIT_COMMIT_COUNT}")
                set(GIT_VERSION_SUCCESS TRUE)
                set(SMPLXMPP_VERSION "${PROJECT_VERSION}-git-${GIT_COMMIT_COUNT}")
                if ("0" EQUAL "${GIT_COMMIT_HASH_RETURN_VAR}")
                    set(SMPLXMPP_VERSION "${SMPLXMPP_VERSION}-${GIT_COMMIT_HASH}")
                else()
                    message(WARNING "SMPLXMPP_VERSION_GUESS_GIT enabled, but git did return commit hash")
                endif()
            else()
                message(WARNING "SMPLXMPP_VERSION_GUESS_GIT enabled, but no additional commits since ${PROJECT_VERSION}, falling back to ${SMPLXMPP_VERSION}")
            endif()
        else()
            message(WARNING "SMPLXMPP_VERSION_GUESS_GIT enabled, but git did not count commits")
        endif()

        execute_process(COMMAND ${GIT_EXECUTABLE} "log" "-1" "--date=short" "--format=%at"
                        WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
                        OUTPUT_VARIABLE GIT_COMMIT_DATE
                        RESULT_VARIABLE GIT_COMMIT_DATE_RETURN_VAR
                        OUTPUT_STRIP_TRAILING_WHITESPACE
                        ERROR_QUIET)

        if ("0" EQUAL "${GIT_COMMIT_DATE_RETURN_VAR}")
            set(SMPLXMPP_VERSION_DATE "${GIT_COMMIT_DATE}")
        else()
            message(WARNING "SMPLXMPP_VERSION_GUESS_GIT enabled, but git did not return commit date, falling back to ${SMPLXMPP_VERSION_DATE}")
        endif()
    else()
        message(WARNING "SMPLXMPP_VERSION_GUESS_GIT enabled, but git executable not found. Falling back to set version ${SMPLXMPP_VERSION}")
    endif()
endif()

# hack to format date
# it aint pretty, but it works

set(SOURCE_DATE_EPOCH_BAK $ENV{SOURCE_DATE_EPOCH})
set(ENV{SOURCE_DATE_EPOCH} "${SMPLXMPP_VERSION_DATE}")

string(TIMESTAMP SMPLXMPP_VERSION_MONTH "%B")
string(TIMESTAMP SMPLXMPP_VERSION_YEAR "%Y")

set(ENV{SOURCE_DATE_EPOCH} "${SOURCE_DATE_EPOCH_BAK}")

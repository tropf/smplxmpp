set(CPACK_GENERATOR "DEB")
set(CPACK_SOURCE_GENERATOR "ZIP;TGZ")

string(TOLOWER ${PROJECT_NAME} PROJECT_NAME_LOWERCASE)

set(CPACK_PACKAGE_NAME ${PROJECT_NAME_LOWERCASE})
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "${PROJECT_DESCRIPTION}")
set(CPACK_PACKAGE_VENDOR "")
set(CPACK_PACKAGE_CONTACT "Rolf Pfeffertal <smplxmpp@tropf.io>")

set(CPACK_RESOURCE_FILE_LICENSE "${PROJECT_SOURCE_DIR}/COPYING")
set(CPACK_RESOURCE_FILE_README "${PROJECT_SOURCE_DIR}/README.md")

set(CPACK_DEBIAN_PACKAGE_SHLIBDEPS YES)

set(CPACK_PACKAGE_VERSION ${SMPLXMPP_VERSION})

include(CPack)

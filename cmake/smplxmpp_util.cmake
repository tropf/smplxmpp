add_library(smplxmpp_util STATIC src/util.cc include/util.h)
target_include_directories(smplxmpp_util PUBLIC
    include/
    )

target_link_libraries(smplxmpp_util PUBLIC "${SMPLXMPP_SPDLOG_LIBS}")

target_compile_features(smplxmpp_util PUBLIC cxx_std_11)
target_compile_options(smplxmpp_util PRIVATE -Werror -Wall -Wextra)

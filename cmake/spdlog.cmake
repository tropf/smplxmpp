
option(SMPLXMPP_SPDLOG_USE_SYSTEM "iff ON, use the spdlog version supplied by the system" ON)

option(SMPLXMPP_SPDLOG_FORCE_HEADER_ONLY "ONLY USED IF SMPLXMPP_SPDLOG_USE_SYSTEM is ON: iff ON, will link against special spdlog header-only-target (may be header-only by default, depends on spdlog version)" OFF)
set(SMPLXMPP_SPDLOG_SRC_PATH "https://github.com/gabime/spdlog/archive/v1.8.2.tar.gz" CACHE STRING "ONLY USED IF SMPLXMPP_SPDLOG_USE_SYSTEM is OFF: path (file or URL) to spdlog source (may be a tarball)")

if (SMPLXMPP_SPDLOG_USE_SYSTEM)
    find_package(spdlog REQUIRED)
    
    if (SMPLXMPP_SPDLOG_FORCE_HEADER_ONLY)
        set(SMPLXMPP_SPDLOG_LIBS "spdlog::spdlog_header_only")
    else()
        set(SMPLXMPP_SPDLOG_LIBS "spdlog::spdlog")
    endif()
else()
    include(ExternalProject)
    ExternalProject_Add(spdlog_external
        CMAKE_ARGS "-DCMAKE_INSTALL_PREFIX=${PROJECT_BINARY_DIR}/extern/spdlog_external"
            "-DSPDLOG_BUILD_ALL=OFF"
            "-DSPDLOG_BUILD_SHARED=OFF"
            "-DSPDLOG_BUILD_EXAMPLE=OFF"
            "-DSPDLOG_BUILD_EXAMPLE_HO=OFF"
            "-DSPDLOG_INSTALL=ON"
            "-DSPDLOG_FMT_EXTERNAL=OFF"
            "-DSPDLOG_FMT_EXTERNAL_HO=OFF"
        PREFIX "${PROJECT_BINARY_DIR}/extern/spdlog_external"
        URL "${SMPLXMPP_SPDLOG_SRC_PATH}"
        )

    add_library(spdlog_interface INTERFACE)
    add_dependencies(spdlog_interface
        spdlog_external
        )

    target_include_directories(spdlog_interface INTERFACE
        "${PROJECT_BINARY_DIR}/extern/spdlog_external/include/"
        )

    set(THREADS_PREFER_PTHREAD_FLAG ON)
    find_package(Threads REQUIRED)

    target_link_libraries(spdlog_interface INTERFACE
        "${PROJECT_BINARY_DIR}/extern/spdlog_external/lib/libspdlog.a"
        Threads::Threads
        )

    # force use header only, as the shared lib that would be linked against is in the build directory (which may be deleted)
    set(SMPLXMPP_SPDLOG_LIBS "spdlog_interface")
endif()


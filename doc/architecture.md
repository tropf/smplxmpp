# Architecture
## Directory Structure
| Directory | Description                 |
|-----------|-----------------------------|
| `bin`     | source code for executables |
| `doc`     | additional documentation    |
| `include` | c++ header files            |
| `man`     | man pages                   |
| `src`     | c++ source code             |

## Libraries
Smplxmpp uses libraries:

- gloox to handle XMPP
- spdlog to handle logging
- argp to handle command line arguments

## Classes and Files
Source files are ommited, only header files are named.

| Class                 | File                    | Responsibilities                                                                                                                                                                         |
|-----------------------|-------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|                       | `bin/smplxmpp-nl.cc`    | io, parsing cmdline options                                                                                                                                                              |
|                       | `bin/smplxmpp.cc`       | io, parsing msgs, passing msgs to handler, handle signals, general init                                                                                                                  |
|                       | `include/cfg_cmdline.h` | define global options (from cmdline/cfgfile), init argp vars, provide functions for argp                                                                                                 |
|                       | `include/cfg_file.h`    | load files/single lines of cfg                                                                                                                                                           |
| `SmplXmppHandlerChat` | `include/chat.cc`       | implement 1-to-1 messaging for `SmplXmppHandlerBase` using gloox api                                                                                                                     |
|                       | `include/config.h.in`   | set constants, pass information from build system                                                                                                                                        |
|                       | `include/glooxtotext.h` | provide helper functions to transform machine-readable classes to human-readable strings                                                                                                 |
| `SmplXmppHandlerMuc`  | `include/muc.h`         | implement muc messaging for `SmplXmppHandlerBase` using gloox api                                                                                                                        |
|                       | `include/util.h`        | provide helper functions (en-/decoding etc.)                                                                                                                                             |
| `SmplXmppHandlerBase` | `include/xmpp.h`        | define various helpers for handling a connection state, provide common functionality for mucs and 1-to-1 chats (connection setup/teardown, main event loop, thread handling, logging...) |
|                       | `man/smplxmpp-nl.1.in`  | man page for usage of `smplxmpp-nl`                                                                                                                                                      |
|                       | `man/smplxmpp.1.in`     | man page for usage of `smplxmpp` binary                                                                                                                                                  |
|                       | `man/smplxmpp.5.in`     | man page for smplxmpp config format                                                                                                                                                      |

## Threads
At any point in time there are two threads running:

1. IO
   - also does setup
   - parse incoming messages
   - enqueues messages using `SmplXmppHandlerBase`
   - detached after setup
2. connection handler
   - receives messages and prints them
   - sends enqueued messages
   
The I/O thread will be quit as soon as the connection handling thread is done.

For signal handling an additional third thread will be launched by the signal.
It will communicate information to connection handling thread.

## Example Sequence
The following sequence describes a scenario where:

- the authentication information is provided by a config file
- some additional command line arguments are present
- a message is sent
- after that a signal is sent and smplxmpp terminated gracefully

This sequence is shortened.

| defined in                          | call                           | purpose                                                                                      |
|-------------------------------------|--------------------------------|----------------------------------------------------------------------------------------------|
| `bin/smplxmpp.cc`                   | `main()`                       | run smplxmpp                                                                                 |
|                                     | `argp_parse()`                 | invoke argp to parse command line arguments                                                  |
| `include/cfg_cmdline.h`             | `parse_opt()`                  | parse individual command line options (invoked by argp, called multiple times)               |
| `include/cfg_file.h`                | `load_cfg_files_once()`        | invoked before the first cmdline options is parsed                                           |
| `include/cfg_file.h`                | `load_cfg_file()`              | loads a cfgfile (here: containing auth info)                                                 |
| `include/chat.h` or `include/muc.h` | constructor                    | init handler for xmpp as `global_handler`; does not open a connection yet                    |
|                                     | `signal()`                     | install signal handlers                                                                      |
| `bin/smplxmpp.cc`                   | `input_cycle()`                | launch & detach input thread                                                                 |
| `include/chat.h` or `include/muc.h` | `global_handler->start()`      | launch xmpp connection & handler                                                             |
| `include/chat.h` or `include/muc.h` | `global_handler->enqueueMsg()` | enqueue message to send                                                                      |
| `include/xmpp.h`                    | `flushMessageQueue()`          | called from event loop started by `global_handler->start()`                                  |
| `include/chat.h` or `include/muc.h` | `sendSingleMsg()`              | called from `flushMessageQueue()`, pass message to gloox api                                 |
| `include/xmpp.h`                    | `global_handler->stop()`       | signal handler calls to stop message                                                         |
| `include/xmpp.h`                    | `teardownHandshakeInit()`      | main event loop is exited, initialize connection teardown (handling of that is omitted here) |
|                                     |                                | `global_handler->start()` is finished                                                        |
| `bin/smplxmpp.cc`                   | `return 0`                     | program finished; implicitly kill io thread                                                  |

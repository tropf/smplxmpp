# Buildsystem
The buildsystem used is cmake.
Use it like any other cmake project, it will probably work.

This is the standard procedure i use:

1. clone repository, change into it
2. create build dir and change to it: `mkdir build; cd build`
3. run cmake: `cmake ..`
4. adjust cmake vars: `ccmake .`
5. build: `make -j8`
6. install: `sudo make install`

Instead of using `sudo make install` you can also build a package.
For that see "Building a Package".

## Variables
These are normal cmake variables.

You can set them interactively by using `ccmake .` in your build directory.
Alernatively specify the variables `VAR` with the value `VALUE` like this when invoking cmake:

```
cmake -DVAR=VALUE ..
```

Specify `-D` multiple times to set multiple variables.

To customize your smplxmpp build, the following variables are at your disposal.

| Name                                | Description                                                                                                                                | Default Value                                            |
|-------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------|
| `BUILD_DOCUMENTATION`               | whether developer documentation should be built                                                                                            | `OFF`                                                    |
| `CMAKE_BUILD_TYPE`                  | type of build (mainly whether to include debug symbols)                                                                                    | `Release`                                                |
| `CMAKE_INSTALL_PREFIX`              | prefix to all stuff smplxmpp install to, typical values apart from the default could be `/opt/smplxmpp`, `~/.local` or `/usr`              | `/usr/local`                                             |
| `SMPLXMPP_BUILD_SMPLXMPP`           | whether to build the smplxmpp binary                                                                                                       | `ON`                                                     |
| `SMPLXMPP_DEFAULT_EXTRA_CACERTS`    | path to additional ca certs file (only one) that should always be loaded                                                                   | empty                                                    |
| `SMPLXMPP_GLOOX_SRC_PATH`           | if the system gloox is **not** used: path (file or URL) to gloox source (may be a tarball)                                                 | `https://camaya.net/download/gloox-1.0.24.tar.bz2`       |
| `SMPLXMPP_GLOOX_SYSTEM_LINKER_FLAG` | if the system gloox is used: linker flag to gloox                                                                                          | `-lgloox`                                                |
| `SMPLXMPP_GLOOX_USE_SYSTEM`         | whether to use gloox provided by the system                                                                                                | `ON`                                                     |
| `SMPLXMPP_GZIP_MAN`                 | gzip man pages before installing (strongly recommended)                                                                                    | `ON`                                                     |
| `SMPLXMPP_INSTALL_SMPLXMPP`         | whether to install the smplxmpp binary. if OFF, the built binary will stay only in the build directory                                     | `ON`                                                     |
| `SMPLXMPP_BUILD_SMPLXMPP_NL`        | whether to build the smplxmpp-nl binary                                                                                                    | `ON`                                                     |
| `SMPLXMPP_INSTALL_SMPLXMPP_NL`      | whether to install the smplxmpp-nl binary. if OFF, the built binary will stay only in the build directory                                  | `ON`                                                     |
| `SMPLXMPP_INSTALL_LICENSE`          | whether smplxmpp should install a license. if your distribution already provides a copy of the GPLv3, you may set this option to false     | `ON`                                                     |
| `SMPLXMPP_INSTALL_MAN`              | whether to install the man pages for smplxmpp (section 1 & 5)                                                                              | `ON`                                                     |
| `SMPLXMPP_INSTALL_MAN_NL`           | whether to install the man page for smplxmpp-nl                                                                                            | `ON`                                                     |
| `SMPLXMPP_LICENSE_INSTALL_DIR`      | directory where the license is installed                                                                                                   | `${CMAKE_INSTALL_PREFIX}/share/doc/smplxmpp`             |
| `SMPLXMPP_LICENSE_INSTALL_FILENAME` | name of the license file                                                                                                                   | `LICENSE`                                                |
| `SMPLXMPP_SPDLOG_USE_SYSTEM`        | iff ON, use the spdlog version supplied by the system (recommended)                                                                        | `ON`                                                     |
| `SMPLXMPP_SPDLOG_FORCE_HEADER_ONLY` | whether to use the target `spdlog::spdlog_header_only` -- which only newer spdlog versions have (only respected when using system spdlog)  | `OFF`                                                    |
| `SMPLXMPP_SPDLOG_SRC_PATH`          | path/URL to spdlog source when not using system spdlog                                                                                     | `https://github.com/gabime/spdlog/archive/v1.8.2.tar.gz` |
| `SMPLXMPP_VERSION_GUESS_GIT`        | whether to guess the version by counting commits since the latest version. otherwise use hard-coded values (defaulting to current release) | `ON`                                                     |
| `SMPLXMPP_VERSION`                  | set to overwrite default version. useful for packaging                                                                                     | guessed (see below)                                      |
| `SMPLXMPP_VERSION_DATE`             | unix timestamp of the current version. will try to get from git if enabled or use hard-coded value of last version                         | `1605552070`                                             |

> All variables are set to sensible defaults: if you don't know what they mean, simply don't touch them and you should be fine.

## Building a Package
Simply execute `make package` after building to create a debian package.
Note that the package respects the `CMAKE_INSTALL_PREFIX`, so despite being a package, all files will go to `/usr/local` by default.

So when building a package, make sure to change the install prefix like so:

```
mkdir build; cd build
cmake -DCMAKE_INSTALL_PREFIX=/usr ..
make -j8
make package
```

## ExternalProject
Note that the gloox source is compiled and downloaded at build time.
Executing `make` requires an active internet connection.

## Version Guessing
When building from a git repo, the repo's tags and the date of the last commit will be used to calculate date and version of smplxmpp.
As fallbacks the data from the latest minor release will be used.
(It is not read from git but loaded from a file, where it is hardcoded.)

The version is constructed as follows:
Get latest version as set in `project()` in `CMakeLists.txt`; here: `$version`.
If the current commit is a tag `v$version` use `$version`, else use `$version-git-$revcnt-$shorthash` with `$revcnt` the count of commits since tag `v$version` and `$shorthash` as the short SHA1 hash of HEAD.

Therefore valid version numbers might be `0.9` or `0.9-git-2-9ea6cb1`.

To bump the version, execute:

- bump version in `CMakeLists.txt` at `project()` cmake-command
- update timestamp in `cmake/git_version.cmake`
- commit & tag the commit `v$version` (replace `$version` with the exact string from `project()` in `CMakeLists.txt`)
- push commit **and tag**

You can disable version guessing by switching `SMPLXMPP_GUESS_GIT_VERSION` to `OFF`.
If you want to specify your own version identifier, for example during packaging, specify `SMPLXMPP_VERSION` and `SMPLXMPP_VERSION_DATE` (as unix timestamp, month & year are calculated during build).

## Linking gloox
By default gloox is linked dynamically from the version provided by the distribution.
By adjusting the build variables it can be downloaded, rebuilt and linked against statically:
Just switch `SMPLXMPP_GLOOX_USE_SYSTEM` to `OFF`, cmake will handle the download and build.

## Linking spdlog
Similarly to gloox you can either use spdlog provided by your system (recommended, default) or download & recompile spdlog during build: Set `SMPLXMPP_SPDLOG_USE_SYSTEM` to `OFF`.

If you use your system spdlog version (recommended), you need spdlog in your include path.

Depending on your spdlog version it may be a header only or a shared library.
If `SMPLXMPP_SPDLOG_FORCE_HEADER_ONLY` is set to `ON`, the special target `spdlog::spdlog_header_only` will be used for linking.

As this target is not available on versions without spdlog as shared library, activating this option will fail on these systems.

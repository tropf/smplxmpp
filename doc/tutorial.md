# Tutorial
A step by step introduction to smplxmpp.

## Prerequisites
You need a XMPP account (a JID and a password) and smplxmpp installed.
In this tutorial "alice@example.com" with the password "hackme" will be used as demonstration.

This tutorial assumes that you are on a fully trusted system, i.e. not a single malicous process is running.

## Setup
Start by testing your configuration:

```
smplxmpp --jid 'alice@example.com' --password 'hackme' -v INFO
```

After a couple of seconds you should see lines ending with:

```
[info] init done, setting up connection...
[info] connected
```

If they don't, provide the server and port with `--server` and `--port`.

This means that your connection is working.
Press Control+D to close it.

> Whenever Control+D, Control+C or `kill` will not stop smplxmpp, press
> Control+C **twice in one second**.
> This will immediately stop smplxmpp.

Using `--password` is a really bad method of passing the password, as it will be visible for every user of the system by listing all processes.
To avoid that create a file named `.smplxmpprc` in your home directory and insert the following content:

```
jid alice@example.com
pass hackme
# uncomment if you need:
# server example.com
# port 1337
```

Immediately protect it from other processes accessing it: `chmod og-rwx ~/.smplxmpprc`
Then run smplxmpp again.

```
smplxmpp -v INFO
```

The output should now mention that file:

```
[info] loading cfg file /home/alice/.smplxmpprc
[info] init done, setting up connection...
[info] connected
```

Quit with Control+D.

Now smplxmpp is working, you can omit `-v INFO`.
There will be no more debugging output (unless something goes wrong).

## Send Your First Messages
To try this out open a XMPP Client and log into a different account.
In this example `alice@example.com` will be controlled by smplxmpp, `bob@example.com` by a different client.

> In theory you could use smplxmpp for both.

### Simple Messages
Start smplxmpp and insert:

```
bob@example.com Hello, World!
```

Don't forget to press enter.
Bob should now receive a message.
Answer the message using Bob's account.
smplxmpp will print:

```
bob@example.com Hello Alice!
```

Quit smplxmpp by pressing Control+D.

### Offline Queue
Now insert some messages into the offline queue.
Alics must **not** be logged in with any other client.

Send a message from Bob to Alice.
**After that** start smplxmpp.
The message should be received from the offline queue and displayed, even though it has been sent in the past.

```
bob@example.com Are you there?
```

Quit smplxmpp by pressing Control+D.

### A First Pipeline
Lets build a first pipeline:
We will prepare a message using echo and send it to Bob.
For a first test execute:

```
echo "bob@example.com Look at my random number: $RANDOM"
```

The output will look like that:

```
bob@example.com Look at my random number: 28881
```

Now we know the generation of the message works, let's send it out.
Just append smplxmpp to the (admittedly very short) pipeline.

```
echo "bob@example.com Look at my random number: $RANDOM" | smplxmpp
```

Bob should now receive a message with random number.

> Note that it takes a couple of seconds to send even a short messages.
> For sending multiple messages it makes much more sense to leave smplxmpp running and feeding it many lines.

## Focus a Single User
In case you want to send and receive messages **only** from Bob, you can "focus" on him.
Try it by launching

```
smplxmpp --focus 'bob@example.com'
```

Now send a message.
Don't add the address in the front this time.

```
Hello old friend!
```

Bob should receive that message.
Now answer from Bob's account.
It will appear without Bob's JID too:

```
Hello Alice, how are you?
```

You can specify `--focus` multiple times.
If you **reveice** a message from **any** of those users, it will be printed -- without their JID.
(This means you **can't tell** who a message is from.)
When sending a message, it will be **sent to all** focused users.

This makes `--focus` only really ideal for a single user.

## Join a MUC
You can join a muc, e.g. `chatroom@conference.example.com`, by using the `--muc` option.
As a nickname, we will use `Alice`.
When joining a muc, you **must add a nickname**.
Otherwise you will get a `StanzaErrorJidMalformed`

```
smplxmpp --muc chatroom@conference.example.com/nick
```

In mucs, the sender's JID will **not be displayed**.

> This is mainly due to the fact that nicks, unlike JIDs, can contain almost all characters.

To send a message, simply type it and hit enter:

```
Hello, guys!
```

When you send a message by using bob's account to the same muc, it will appear in smplxmpp:

```
hi i'm bob and i'm new here. i like mountainbiking and cryptography.
```

Try to send a message to alice directly:
It will not be displayed.
Also you can't send messages to Bob (or any other account) while you are in a muc.

You can only join **one muc at a time**.

## Handling Line Breaks
In smplxmpp, **one line** always contains **one message**.
This means that messages with multiple lines will use escaped newline characters.
Every newline will be converted to `\n`.
Ever single backslash `\` will be converted to a double backslash `\\`.
**No other characters are escaped.**

## Decoding Messages
Try it by sending a multi-line message from bob:

```
Hey Alice,
I found out that there are two types of slashes. a / and a \.
I really like to use them as arms like so \o/
cya
-Bob
```

smplxmpp will display it like this:

```
bob@example.com Hey Alice,\nI found out that there are two types of slashes. a / and a \\.\nI really like to use them as arms like so \\o/\ncya\n-Bob
```

To convert the message back, you can pipe it through `smplxmpp-nl` with the `-d` flag to decode a message:

```
smplxmpp | smplxmpp-nl -d
```

The output will be:

```
bob@example.com Hey Alice,
I found out that there are two types of slashes. a / and a \.
I really like to use them as arms like so \o/
cya
-Bob
```

Note that the JID is still in front.
In general it makes more sense to use smplxmpp-nl together with `--focus` or later in the pipeline when the prefixed JID has already been removed.

Also note how smplxmpp-nl will print an additional newline after it is done decoding.
This behaviour of adding a newline is not that desirable for decoding, so you can disable it by using the flag `-n`.

## Encoding Messages
smplxmpp-nl can also be used when sending messages.
For example in a pipeline that prints multi-line output use it like so:

```
fortune | smplxmpp-nl | smplxmpp --focus 'bob@example.com'
```

The output will arrive in a single multi-line message:

```
Q:	Why should you always serve a Southern Carolina football man
	soup in a plate?
A:	'Cause if you give him a bowl, he'll throw it away.
```

## More Complicated Configuration Files
You can use all long command line option also in configuration files.
Let's create a simple example using three files:

- `auth.cfg` containing authentication
- `bob.cfg` for focusing on bob@example.com
- `muc.cfg` to join `chatroom@conference.example.com` with the nickname `bot`. You also want to create a log file for debugging.

Then you would create `auth.cfg`:

```
jid alice@example.com
password hackme
```

`bob.cfg`

```
cfg-file auth.cfg
focus bob@example.com
```

`muc.cfg`

```
cfg-file auth.cfg
muc chatroom@conference.example.com/bot
logfile-debug /tmp/chatroom.debug.log
```

Now to focus on Bob, simply launch:

```
smplxmpp --cfg-file bob.cfg
```

And to join the chatroom:

```
smplxmpp --cfg-file muc.cfg
```

## Some Simple Pipelines
### File content
To send file content, use:

```
cat /proc/loadavg | smplxmpp-nl | smplxmpp --focus bob@example.com 
```

### Greeter Bot
This bot will greet people in a MUC:

```
smplxmpp --no-input --muc 'chatroom@conference.example.com/bot-ears' |
sed -uEne '/^[Hh]i!?$/p' |
sed -uEe 's/^.*$/How are you?/g' |
smplxmpp --no-output --muc 'chatroom@conference.example.com/bot-mouth'
```

What happens is, that only messages that are "Hi" or "Hi!" will get forwarded to the second `sed`, which then replaces them by "How are you?" and echoes them to the second smplxmpp -- which will send them to the muc.

Note that sed is run in unbuffered mode using the `-u` flag.

Also note that the bot joins twice, once to send and once to receive -- but using different nicks.

This example does not utilise `smplxmpp-nl` as only single-line messages are handled.

> During testing Dino.im rejected the same message multiple times, while conversation displayed it always.
> So make sure to check with multiple clients.

This example must be stopped by pressing Control+C.
Control+D will not work, as `--no-input` also disabled parsing the EOF character produced by Control+D.

### The "default pipeline"
You can use this pipeline to connect any command's `stdin` to incoming messages and `stdout` to outgoing messages.
Note that in this case the used command has to do all the decoding itself.

```
mkfifo /tmp/feedback
stdbuf -o0 cat /tmp/feedback | stdbuf -i0 -o0 COMMAND | stdbuf -i0 -o0 smplxmpp > /tmp/feedback
```

The pipeline can be adjusted to include debugging (add `-v DEBUG`), focus on a specific user (add `-f "alice@example.com"`) or use a regular file (omit `mkfifo` and use `tail -f` instead of `cat`).

### Play adventure
Using a variation of the "default pipeline" above, you can play `adventure` via XMPP:

> And if you play it, remember that the rod scares the bird.

```
stdbuf -o0 tail -f /tmp/f | stdbuf -i0 -o0 adventure | stdbuf -i0 -o0 smplxmpp -f 'alice@example.com' -v DEBUG > /tmp/f
```

#ifndef __SMPLXMPP_CHAT_H_INCLUDED__
#define __SMPLXMPP_CHAT_H_INCLUDED__

#include <xmpp.h>

#include <gloox/client.h>
#include <gloox/disco.h>
#include <gloox/discohandler.h>
#include <gloox/gloox.h>
#include <gloox/messageeventfilter.h>
#include <gloox/messageeventhandler.h>
#include <gloox/messagehandler.h>
#include <gloox/messagesessionhandler.h>

using namespace std;
using namespace gloox;

/**
 * handles 1-to-1 chat sessions
 */
class SmplXmppHandlerChat : public SmplXmppHandlerBase, MessageSessionHandler, MessageEventHandler, MessageHandler, DiscoHandler {
    protected:
        /**
         * represents the conversation between us and a partner.
         * concept from gloox.
         */
        MessageSession* m_session;

        /**
         * responsible for handlers.
         */
        MessageEventFilter* m_messageEventFilter;

        /**
         * saves if we already tried to enable carbons.
         */
        bool carbonsCheckedForExistence = false;

        /**
         * sends out a single message.
         * generally called from flushMessageQueue().
         * @param msg the message to send
         */
        virtual void sendSingleMsg(const MsgPrototype& msg);

    public:
        /**
         * Constructor.
         * Connects all necessary handlers and does other init.
         * Does not open a server connection.
         * @param jid JID to use for auth
         * @param pass password to use for auth
         * @param given_cfg config to be used
         */
        SmplXmppHandlerChat(JID jid, string& pass, const SmplXmppHandlerConfig& given_cfg = smplXmppHandlerConfigDefault);

        /**
         * destructor
         */
        ~SmplXmppHandlerChat() {}

        /**
         * enables carbons after establishing a connection.
         */
        virtual void onConnect();

        /**
         * does quite a lot of checks before printing the message to stdout.
         * printed message will use encoding for newlines and backslashes
         *
         * @param msg incoming message stanza
         * @param session current message session
         */
        virtual void handleMessage(const Message& msg, MessageSession* session);

        /**
         * just implemented to satisfy spec.
         * simply logs, does nothing.
         * @param from sender
         * @param event thing that happened
         */
        virtual void handleMessageEvent(const JID& from, MessageEventType event);

        /**
         * simple script to satisfy gloox api.
         * don't pay attention, simply copied from gloox examples.
         * @param session new seesion thingy
         */
        virtual void handleMessageSession(MessageSession* session);

        /**
         * used to check if carbons are supported.
         * if so, will enable them via IQ.
         *
         * @param from sender
         * @param info actual stanza information
         * @param context context param (can basically be ignored)
         */
        virtual void handleDiscoInfo( const JID& from, const Disco::Info& info, int context);

        /**
         * only logs.
         * exists to satisfy gloox api
         * @param from sender
         * @param items items
         * @param context context param (can basically be ignored)
         */
        virtual void handleDiscoItems( const JID& from, const Disco::Items& items, int context);

        /**
         * logs any errors
         * @param from sender
         * @param error occured error
         * @param context context param (can basically be ignored)
         */
        virtual void handleDiscoError( const JID& from, const Error* error, int context);
};

#endif // __SMPLXMPP_CHAT_H_INCLUDED__

#ifndef __SMPLXMPP_MUC_H_INCLUDED__
#define __SMPLXMPP_MUC_H_INCLUDED__

#include <list>

#include <gloox/gloox.h>
#include <gloox/mucroom.h>
#include <gloox/mucroomhandler.h>

#include <xmpp.h>

using namespace std;
using namespace gloox;

/**
 * handles connections to mucs
 */
class SmplXmppHandlerMuc : public SmplXmppHandlerBase, MUCRoomHandler {
    protected:
        /**
         * muc representation from gloox
         */
        MUCRoom m_room;

        /**
         * holds recently sent messages to the muc, so we can ignore them when we receive them back from the server.
         *
         * just acts as a buffer.
         * sent messages will be enqueued, if we receive messages back they will be dequeued again.
         */
        list<string> msgsSentByMe;

        /**
         * sends out a single message.
         * generally called from flushMessageQueue().
         * JID part given in the msg is completly ignored.
         * @param msg the message to send
         */
        virtual void sendSingleMsg(const MsgPrototype& msg);
    public:
        /**
         * Constructor.
         * Connects all necessary handlers and does other init.
         * Does not open a server connection.
         * @param jid JID to use for auth
         * @param pass password to use for auth
         * @param given_cfg config to be used
         */
        SmplXmppHandlerMuc(JID jid, string& pass, const SmplXmppHandlerConfig& given_cfg = smplXmppHandlerConfigDefault);

        /**
         * destructor
         */
        ~SmplXmppHandlerMuc() {}

        /**
         * joins the muc on connection establishment
         */
        virtual void onConnect();

        /**
         * does plenty of checks to see if the stream is ready to send/recv messages
         * also checks if we can write to the muc
         * @return if we can write messages
         */
        virtual bool isStreamReady();

        /**
         * handles received messages
         * @param msg received message
         * @param priv true if private message
         */
        virtual void handleMUCMessage(MUCRoom* /*room*/, const Message& msg, bool priv);

        /**
         * handles a muc error.
         * actually doesn't do anything, just prints to log and continues
         * @param error error to log
         */
        virtual void handleMUCError(MUCRoom * /*room*/, StanzaError error);

        /**
         * used to log if a muc does not exist on joining (and will be created)
         * @param room created muc
         */
        virtual bool handleMUCRoomCreation(MUCRoom *room);

        /**
         * does nothing.
         * only implemented to satisfy gloox api
         * @param participant param that we ignore
         * @param presence other param that we ignore
         */
        virtual void handleMUCParticipantPresence(MUCRoom * /*room*/, const MUCRoomParticipant participant, const Presence& presence);

        /**
         * saves muc subject to log
         * @param nick ignored
         * @param subject subject that has been set
         */
        virtual void handleMUCSubject(MUCRoom * /*room*/, const std::string& nick, const std::string& subject);

        /**
         * saves muc info to log
         * @param features features
         * @param name name
         * @param infoForm infoForm
         */
        virtual void handleMUCInfo(MUCRoom * /*room*/, int features, const std::string& name, const DataForm* infoForm);

        /**
         * does nothing
         * only exists to satisfy gloox api
         * @param items ignored
         */
        virtual void handleMUCItems(MUCRoom * /*room*/, const Disco::ItemList& items);

        /**
         * does nothing
         * only exists to satisfy gloox api
         * @param invitee ignored
         * @param reason also ignored
         */
        virtual void handleMUCInviteDecline(MUCRoom * /*room*/, const JID& invitee, const std::string& reason);
};

#endif // __SMPLXMPP_MUC_H_INCLUDED__

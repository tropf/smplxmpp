#ifndef __SMPLXMPP_UTIL_H_INCLUDED__
#define __SMPLXMPP_UTIL_H_INCLUDED__

#include <string>
#include <spdlog/spdlog.h>

using namespace std;

/**
 * equivalent to sed -e 's/\n/\\n/g' -e 's/\\/\\\\/g'
 * required to print multi-line messages in a single line
 * @param s string with newlines and backslashes
 * @return s with \n and \ escaped
 */
string newlinesEncode(const string& s);

/**
 * reverses newlincesEncode
 * required to read multiline messages from stdin on a single line
 * @param s string with escaped newlines and backslashes
 * @return s with unescaped \n and \
 */
string newlinesDecode(const string& s);

/**
 * extracts the current home dir.
 * Tries to use $HOME first, then the actual users home directoy
 * @return home directory
 */
string getHomeDir();

/**
 * required as struct argp_option did not overload operator==
 * @param o argp_option to check
 * @return whether o is empty (only NULL)
 */
bool is_empty_argp_option(const struct argp_option& o);

/**
 * inits spdlog to handle logs of every level, but only print warn to stderr
 */
void initSpdlog();

/**
 * set the default log level of stderr.
 * call after initSpdlog()
 * @param lvl min required to log level to print msg
 */
void setStderrLoglevel(spdlog::level::level_enum lvl);

/**
 * get human-readable string from seconds since epoch
 * @param t seconds since epoch
 * @return human-readable representation of timepoint in localtime
 */
string getHumanTimePointFromEpoch(int t);

/**
 * get same string with all characters in upper case
 * @param s string to be transformed
 * @return s with all lower case letters replaced by upper case
 */
string getUppercase(string s);

#endif // __SMPLXMPP_UTIL_H_INCLUDED__

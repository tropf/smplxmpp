#include <xmpp.h>

#include <chrono>
#include <iostream>
#include <thread>
#include <algorithm>
#include <regex>
#include <mutex>

#include <spdlog/spdlog.h>

#include <gloox/client.h>
#include <gloox/carbons.h>
#include <gloox/disco.h>
#include <gloox/error.h>
#include <gloox/forward.h>
#include <gloox/gloox.h>
#include <gloox/iq.h>
#include <gloox/message.h>
#include <gloox/mucroom.h>
#include <gloox/stanza.h>
#include <gloox/stanzaextension.h>

#include <util.h>
#include <config.h>
#include <glooxtotext.h>

using namespace std;
using namespace gloox;

SmplXmppHandlerBase::SmplXmppHandlerBase(JID jid, string& pass, const SmplXmppHandlerConfig& given_cfg):
    j(jid, pass) {
    cfg = given_cfg;

    j.setTls(cfg.tlsPolicy);

    if ("" != cfg.server) {
        j.setServer(cfg.server);
    }

    if (0 != cfg.port) {
        j.setPort(cfg.port);
    }

    string extraCACerts(SMPLXMPP_DEFAULT_EXTRA_CACERTS);
    if (!cfg.caCerts.empty() || !extraCACerts.empty()) {
        auto certs = cfg.caCerts;

        if (!cfg.caCerts.empty()) {
            spdlog::info("adding {} custom CA certs", cfg.caCerts.size());
            certs = cfg.caCerts;
        }

        if (!extraCACerts.empty()) {
            spdlog::debug("adding default ca certs");
            certs.push_back(extraCACerts);
        }

        j.setCACerts(certs);
    }

    j.registerConnectionListener(this);
    j.registerIqHandler(this, StanzaExtensionType::ExtError);
    j.disco()->setVersion("SmplXMPP", SMPLXMPP_VERSION, "Linux");
    j.disco()->setIdentity("client", "bot");

    j.logInstance().registerLogHandler(LogLevelDebug, LogAreaAll, this);

    keepRunning = true;
    teardownHandshakeState = TeardownHandshakeState::NotStarted;
}

void SmplXmppHandlerBase::start() {
    if (!j.connect(false)) {
        spdlog::error("couldn't establish connection");
        return;
    }

    // reset time to last heartbeat: we just sent data, treat this as a heartbeat/activity during setup
    lastHeartbeat = chrono::steady_clock::now();
    lastSetupActivity = chrono::steady_clock::now();

    spdlog::debug("waiting for established connection");

    // wait for established connection
    ConnectionError ce = ConnNoError;
    while(!isStreamReady() && ce == ConnNoError) {
        ce = j.recv(cfg.timeoutFlush);
        sendHeartbeatMaybe();
        setupFlushMaybe();
    }

    spdlog::trace("entering core loop");

    // actual core loop
    while (keepRunning && ce == ConnNoError) {
        flushMessageQueue();
        sendHeartbeatMaybe();
        ce = j.recv(cfg.timeoutFlush);
    }

    spdlog::trace("exited core loop");

    if (ce != ConnNoError) {
        spdlog::error("connection error occured: {}", glooxErrorToText(ce));
    } else {
        // we want to stop, but connection is still open
        auto shutdownInitiated = chrono::steady_clock::now();

        // flush remaining messages
        flushMessageQueue();

        // problem: gloox will teardown the connection, even if there is data not sent
        // solution: send a request to the server, only teardown after it has been acknowledged
        teardownHandshakeInit();

        ConnectionError te = ConnNoError;
        unsigned usSinceShutdownInitiated = 0;
        while (!teardownHandshakeDone() &&
               te == ConnNoError &&
               usSinceShutdownInitiated <= cfg.timeoutTeardown) {
            te = j.recv(cfg.timeoutFlush);
            usSinceShutdownInitiated = chrono::duration_cast<chrono::microseconds>(chrono::steady_clock::now() - shutdownInitiated).count();
        }

        if (te != ConnNoError) {
            spdlog::error("connection error occured during connection teardown: {}", glooxErrorToText(ce));
            return;
        }

        j.disconnect();

        lock_guard<mutex> lock(msgQueueMutex);
        if (0 < msgQueue.size()) {
            spdlog::error("closed, remaining msgs in queue: {}", msgQueue.size());
        } else {
            spdlog::info("closed, msg queue empty");
        }
    }
}

void SmplXmppHandlerBase::stop() {
    spdlog::info("graceful shutdown initiated");
    keepRunning = false;
}

void SmplXmppHandlerBase::onConnect() {
    spdlog::info("connected");
}

void SmplXmppHandlerBase::onDisconnect(ConnectionError e) {
    if (e == ConnNoError) {
        spdlog::info("clean disconnect");
    } else if (e == ConnAuthenticationFailed) {
        spdlog::warn("authentication failed, reason: {}", glooxErrorToText(j.authError()));
    } else if (e == ConnUserDisconnected) {
        spdlog::info("client disconnect done");
    } else {
        spdlog::warn("disconnected due to error {}", glooxErrorToText(e));
    }
}

bool SmplXmppHandlerBase::onTLSConnect(const CertInfo& info) {
    if (CertOk != info.status) {
        if (!cfg.tlsIgnoreCertCheck) {
            // default operation
            spdlog::warn("TLS certificate rejected");
        } else {
            spdlog::warn("TLS certificate not valid; accepting anyways");
        }
    }

    spdlog::debug("TLS cert info");
    spdlog::debug("* status:      {}", info.status);
    spdlog::debug("* issuer:      {}", info.issuer);
    spdlog::debug("* peer:        {}", info.server);
    spdlog::debug("* protocol:    {}", info.protocol);
    spdlog::debug("* mac:         {}", info.mac);
    spdlog::debug("* cipher:      {}", info.cipher);
    spdlog::debug("* compression: {}", info.compression);
    spdlog::debug("* from:        {}", getHumanTimePointFromEpoch(info.date_from));
    spdlog::debug("* to:          {}", getHumanTimePointFromEpoch(info.date_to));

    return (info.status == CertOk) || cfg.tlsIgnoreCertCheck;
}

bool SmplXmppHandlerBase::isStreamReady() {
    return streamReady && j.authed() && ConnectionState::StateConnected == j.state();
}

void SmplXmppHandlerBase::tidyMessageQueue() {
    lock_guard<mutex> lock(msgQueueMutex);

    list<MsgPrototype> tidied;
    for (const auto& msg : msgQueue) {
        if (msg.body.empty()) {
            spdlog::debug("discarding message with empty body to '{}'", msg.jid);
            continue;
        }

        tidied.push_back(msg);
    }

    msgQueue = tidied;
}

void SmplXmppHandlerBase::flushMessageQueue() {
    tidyMessageQueue();

    lock_guard<mutex> lock(msgQueueMutex);
    while(!msgQueue.empty() && isStreamReady()) {
        sendSingleMsg(msgQueue.front());
        msgQueue.pop_front();
    }
}

void SmplXmppHandlerBase::sendSingleMsg(const MsgPrototype&) {
    spdlog::error("Tried to send message via base handler. (which does not work.)");
}

void SmplXmppHandlerBase::onStreamEvent(StreamEvent event) {
    if (StreamEvent::StreamEventFinished == event) {
        streamReady = true;
    }

    // "Connecting" indicates "about to connect" -> any other event is actual activity
    if (StreamEvent::StreamEventConnecting != event) {
        lastSetupActivity = chrono::steady_clock::now();
    }
}

void SmplXmppHandlerBase::handleLog(LogLevel level, LogArea area, const std::string& message) {
    switch (level) {
        case LogLevel::LogLevelDebug:  spdlog::trace("[area {}] {}", glooxLogAreaToText(area), message); break;
        case LogLevel::LogLevelWarning: spdlog::warn("[area {}] {}", glooxLogAreaToText(area), message); break;
        case LogLevel::LogLevelError:  spdlog::error("[area {}] {}", glooxLogAreaToText(area), message); break;
    }
}

void SmplXmppHandlerBase::enqueueMsg(MsgPrototype msg) {
    if (teardownHandshakeStarted()) {
        spdlog::warn("tried to enqueue msg after shutdown has been initiated. ignoring");
        return;
    }
    if (cfg.titForTatMode) {
        if (titForTatRecipientQueue.empty()) {
            spdlog::error("could not enqueue message: no recipients in tit for tat queue");
            return;
        }

        lock_guard<mutex> lock(titForTatRecipientQueueMutex);
        msg.jid = titForTatRecipientQueue.front().bare();
        titForTatRecipientQueue.pop_front();

        spdlog::debug("{} recipients in tit-for-tat queue", titForTatRecipientQueue.size());
    }

    spdlog::trace("enqueued message to send to '{}': '{}'", msg.jid, msg.body);

    lock_guard<mutex> lock(msgQueueMutex);
    msgQueue.push_back(msg);
}

void SmplXmppHandlerBase::enqueueMsg(const string& jid, const string& body) {
    enqueueMsg({jid, body});
}

bool SmplXmppHandlerBase::handleIq(const IQ& iq) {
    // check if this iq is a response to the teardown handshake
    spdlog::trace("received IQ: '{}'", iq.id());
    if (TeardownHandshakeState::IQSent == teardownHandshakeState) {
        if (iq.id() == teardownHandshakeIQID) {
            teardownHandshakeState = TeardownHandshakeState::Done;
        }
    }

    return true;
}

void SmplXmppHandlerBase::handleIqID(const IQ&, int) {
    // empty, just here to satisfy gloox API
}

void SmplXmppHandlerBase::teardownHandshakeInit() {
    teardownHandshakeIQID = j.getID();
    spdlog::debug("initializing teardown handshake w/ IQ ID '{}'", teardownHandshakeIQID);
    IQ faultyIq(IQ::IqType::Get, j.jid().server(), teardownHandshakeIQID);
    j.send(faultyIq);

    teardownHandshakeState = TeardownHandshakeState::IQSent;
}

bool SmplXmppHandlerBase::teardownHandshakeStarted() {
    return TeardownHandshakeState::NotStarted != teardownHandshakeState;
}

bool SmplXmppHandlerBase::teardownHandshakeDone() {
    return TeardownHandshakeState::Done == teardownHandshakeState;
}

void SmplXmppHandlerBase::sendHeartbeatMaybe() {
    if (0 == cfg.heartbeatInterval) {
        return;
    }

    auto now = chrono::steady_clock::now();
    auto diff = now - lastHeartbeat;
    unsigned int diff_secs = chrono::duration_cast<chrono::seconds>(diff).count();

    if (diff_secs >= cfg.heartbeatInterval) {
        // elapsed time > allowed interval
        // -> send heartbeat
        spdlog::trace("sending heartbeat");
        j.whitespacePing();

        // save time of heartbeat
        lastHeartbeat = chrono::steady_clock::now();
    }
}

void SmplXmppHandlerBase::setupFlushMaybe() {
    if (0 == cfg.timeoutSetupFlush) {
        return;
    }

    auto now = chrono::steady_clock::now();
    auto diff = now - lastSetupActivity;
    unsigned int diff_us = chrono::duration_cast<chrono::microseconds>(diff).count();

    if (diff_us >= cfg.timeoutSetupFlush) {
        spdlog::debug("attempting to flush the connection");

        // send a faulty IQ to provoke server reaction == traffic
        Tag* brokenIq;
        brokenIq = new Tag("iq", "type", "smplxmpp");
        // note: send() will destroy the Tag
        j.send(brokenIq);

        // save time of flush attempt
        lastSetupActivity = chrono::steady_clock::now();
    }
}
